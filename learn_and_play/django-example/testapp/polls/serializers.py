from rest_framework import serializers
# from django.utils import timezone
from polls.models import Question
from django.contrib.auth.models import User


class QuestionSerializer(serializers.ModelSerializer):
    author = serializers.ReadOnlyField(source="author.username")

    class Meta:
        model = Question
        fields = ["id", "question_text", "pub_date", "author"]


class UserSerializer(serializers.ModelSerializer):
    questions = serializers.PrimaryKeyRelatedField(
        many=True, queryset=Question.objects.all()
    )

    class Meta:
        model = User
