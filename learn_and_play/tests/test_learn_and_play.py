import pytest


@pytest.mark.parametrize('num1, num2, expected',
                         [(1, 2, 3), (2, 3, 5), (4, 4, 8), (5, 5, 10)])
def test_adding(num1, num2, expected):
    assert num1 + num2 == expected
