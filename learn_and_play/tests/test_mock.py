import unittest.mock as mock
import pytest


class SomeClass:
    a = 1
    b = 2
    c = 'a'

    def __init__(self, d):
        self.d = d

    def e(self):
        return 'some answer'

    def f(self):
        return {'some': 'arg', 'useless': 'arg'}

    def g(self):
        pass


some_instance = SomeClass(3.2)
some_instance.method1 = mock.Mock(return_value=42)
some_instance.method2 = mock.Mock(side_effect=TypeError)


def test_mocked_method():
    """
    test of mocked return value
    :return:
    """
    assert some_instance.method1() == 6 * 7


def test_mocked_side_effect():
    with pytest.raises(TypeError):
        some_instance.method2('some', 1, 'args')
