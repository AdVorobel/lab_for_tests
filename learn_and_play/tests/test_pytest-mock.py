import math
import os
import time

from pytest_mock import MockerFixture

USERNAMES = ['ailof', 'dendra', 'kurtis']
VERIFIED_ACCOUNTS = []


def auth(username):
    # some complicated actions
    time.sleep(5)
    return username in USERNAMES


def verify_acc(account):
    if auth(account):
        VERIFIED_ACCOUNTS.append(account)
    return False


def test_mock_verify_account(mocker):
    mocker.patch(__name__ + '.auth', return_value=False)
    verify_acc('ailof')
    assert 'ailof' not in VERIFIED_ACCOUNTS


# def test_verify_account():
#     verify_acc('ailof')
#     assert 'ailof' in VERIFIED_ACCOUNTS


def test_mock_environ(mocker):
    mocker.patch.dict('os.environ', {'connect': 'some_url', 'endpoint': 'some_endpoint'})
    assert f"https://{os.environ['connect']}/{os.environ['endpoint']}" == "https://some_url/some_endpoint"


def test_mock_smth(mocker: MockerFixture):
    mock_obj = mocker.spy(math, 'pow')
    assert math.pow(2, 3) == 8
    mock_obj.assert_called_once_with(2, 3)
    assert round(math.pow(2, -5), 3) == round(1 / 32, 3)
    assert mock_obj.call_count > 1
