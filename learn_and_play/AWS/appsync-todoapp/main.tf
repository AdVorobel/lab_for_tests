provider "aws" {
  region = "eu-central-1"
}

resource "aws_appsync_graphql_api" "my_appsync_api" {
  authentication_type = "API_KEY"
  name                = "my_appsync_api"

  schema = file("${path.module}/data/schema.graphql")

  tags = {
    Name = "todoApp_appsync"
  }
}

resource "aws_appsync_api_key" "my_appsync_api_key" {
  api_id = aws_appsync_graphql_api.my_appsync_api.id
}

resource "aws_dynamodb_table" "my_table" {
  hash_key       = "id"
  name           = "table_todo_app"
  billing_mode   = "PROVISIONED"
  read_capacity  = 1
  write_capacity = 1
  attribute {
    name = "id"
    type = "N"
  }
}