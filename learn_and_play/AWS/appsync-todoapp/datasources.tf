resource "aws_appsync_datasource" "appsync_data" {
  api_id = aws_appsync_graphql_api.my_appsync_api.id
  name = "tf_appsync_data_source"
  type = "AMAZON_DYNAMODB"
  service_role_arn = aws_iam_role.dynamo_role.arn

  dynamodb_config {
    table_name = aws_dynamodb_table.my_table.name
  }
}

resource "aws_iam_role" "dynamo_role" {
  name = "appsync_dynamodb_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "appsync.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "dynamo_role_policy" {
  name = "appsync_dynamodb_fullAccess"
  role = aws_iam_role.dynamo_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "dynamodb:*"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_dynamodb_table.my_table.arn}"
      ]
    }
  ]
}
EOF
}