resource "aws_appsync_resolver" "add_todo_resolver" {
  api_id            = aws_appsync_graphql_api.my_appsync_api.id
  field             = "addToDo"
  request_template  = "{}"
  response_template = "$util.toJson($ctx.result)"
  type              = "Mutation"
  kind              = "PIPELINE"
  pipeline_config {
    functions = [
      aws_appsync_function.add_todo.function_id,
    ]
  }
}

resource "aws_appsync_resolver" "delete_todo_resolver" {
  api_id            = aws_appsync_graphql_api.my_appsync_api.id
  field             = "deleteToDo"
  request_template  = "{}"
  response_template = "$util.toJson($ctx.result)"
  type              = "Mutation"
  kind              = "PIPELINE"
  pipeline_config {
    functions = [
      aws_appsync_function.delete_todo.function_id,
    ]
  }
}

resource "aws_appsync_resolver" "get_todo_resolver" {
  api_id            = aws_appsync_graphql_api.my_appsync_api.id
  field             = "getSingleToDo"
  request_template  = "{}"
  response_template = "$util.toJson($ctx.result)"
  type              = "Query"
  kind              = "PIPELINE"
  pipeline_config {
    functions = [
      aws_appsync_function.get_todo.function_id,
    ]
  }
}

resource "aws_appsync_resolver" "get_todo_list_resolver" {
  api_id            = aws_appsync_graphql_api.my_appsync_api.id
  field             = "getToDolist"
  request_template  = "{}"
  response_template = "$util.toJson($ctx.result)"
  type              = "Query"
  kind              = "PIPELINE"
  pipeline_config {
    functions = [
      aws_appsync_function.get_todo_list.function_id,
    ]
  }
}

resource "aws_appsync_resolver" "update_resolver" {
  api_id            = aws_appsync_graphql_api.my_appsync_api.id
  field             = "updateToDo"
  request_template  = "{}"
  response_template = "$util.toJson($ctx.result)"
  type              = "Mutation"
  kind              = "PIPELINE"
  pipeline_config {
    functions = [
      aws_appsync_function.update_todo.function_id,
    ]
  }
}
