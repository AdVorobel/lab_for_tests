resource "aws_appsync_function" "add_todo" {
  api_id                    = aws_appsync_graphql_api.my_appsync_api.id
  data_source               = aws_appsync_datasource.appsync_data.name
  name                      = "add_todo"
  request_mapping_template  = file("${path.module}/functions/add_todo/req.vtl")
  response_mapping_template = file("${path.module}/functions/add_todo/resp.vtl")
}

resource "aws_appsync_function" "delete_todo" {
  api_id                    = aws_appsync_graphql_api.my_appsync_api.id
  data_source               = aws_appsync_datasource.appsync_data.name
  name                      = "delete_todo"
  request_mapping_template  = file("${path.module}/functions/delete_todo/req.vtl")
  response_mapping_template = file("${path.module}/functions/delete_todo/resp.vtl")
}

resource "aws_appsync_function" "get_todo" {
  api_id                    = aws_appsync_graphql_api.my_appsync_api.id
  data_source               = aws_appsync_datasource.appsync_data.name
  name                      = "get_todo"
  request_mapping_template  = file("${path.module}/functions/get_todo/req.vtl")
  response_mapping_template = file("${path.module}/functions/get_todo/resp.vtl")
}

resource "aws_appsync_function" "get_todo_list" {
  api_id                    = aws_appsync_graphql_api.my_appsync_api.id
  data_source               = aws_appsync_datasource.appsync_data.name
  name                      = "get_todo_list"
  request_mapping_template  = file("${path.module}/functions/get_todo_list/req.vtl")
  response_mapping_template = file("${path.module}/functions/get_todo_list/resp.vtl")
}

resource "aws_appsync_function" "update_todo" {
  api_id                    = aws_appsync_graphql_api.my_appsync_api.id
  data_source               = aws_appsync_datasource.appsync_data.name
  name                      = "update_todo"
  request_mapping_template  = file("${path.module}/functions/update_todo/req.vtl")
  response_mapping_template = file("${path.module}/functions/update_todo/resp.vtl")
}