import json

from chalice import Chalice

app = Chalice(app_name='lambda-chalice')


@app.lambda_function(name='simple_calculator')
def lambda_handler(event, context):
    if event['action'] == 'add':
        result = event['x'] + event['y']
    elif event['action'] == 'mult':
        result = event['x'] * event['y']
    elif event['action'] == 'div':
        result = event['x'] / event['y']
    elif event['action'] == 'sub':
        result = event['x'] - event['y']
    else:
        return {
            'message': 'Unknown action'
        }
    return {
        'result': json.dumps(result)
    }
