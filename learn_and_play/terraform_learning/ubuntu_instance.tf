provider "aws" {
  region = "eu-central-1"
}

resource "aws_instance" "my_instance" {
  ami           = "ami-090f10efc254eaf55"
  instance_type = "t3.micro"
}