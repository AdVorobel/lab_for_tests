#!/home/adrian/.pyenv/shims/python

import sys
import subprocess


def main():
    print(' step 1 '.center(72, '#'))
    print('--> running pytest')

    try:
        output = subprocess.check_output('pytest')
        print(output.decode('utf-8'))
    except subprocess.CalledProcessError as e:
        print(' error '.center(72, '#'))
        print(e.output.decode('utf-8'), file=sys.stderr)
        sys.exit(1)
    else:
        print('--> pytest passed')

    print(' step 2 '.center(72, '#'))
    print('--> running flake8')

    try:
        output = subprocess.check_output('flake8')
        print(output.decode('utf-8'))
    except subprocess.CalledProcessError as e:
        print(' error '.center(72, '#'))
        print(e.output.decode('utf-8'), file=sys.stderr)
        sys.exit(1)
    else:
        print('--> flake8 passed')

    sys.exit(0)


if __name__ == "__main__":
    main()
