import sys

from flask import Flask
import sqlite3

app = Flask(__name__)

try:
    db = sqlite3.connect('./mydb.db')
    print(sqlite3.version)
except sqlite3.Error as e:
    print(e)
    sys.exit(1)

try:
    cursor = db.cursor()
    cursor.execute(""" CREATE TABLE IF NOT EXISTS dummy (
                                        id integer PRIMARY KEY AUTOINCREMENT,
                                        text text NOT NULL
                                    ); """)
    cursor.close()
    print('table created')
except sqlite3.Error as e:
    print(e)


@app.route('/')
def hello_world():
    return 'Hello, World!'


@app.route('/<name>')
def hello_world2(name):
    if name != 'favicon.ico':
        db = sqlite3.connect('./mydb.db')
        cursor = db.cursor()
        cursor.execute(f""" INSERT INTO dummy(text) VALUES ("{name}"); """)
        try:
            db.commit()
        except sqlite3.Error as e:
            print(e)

        cursor.execute("SELECT text FROM dummy")

        rows = cursor.fetchall()
        rows = [i[0] for i in rows]
        cursor.close()
        db.close()
        rows = "\t".join(rows)
        return f'Hello, {name}!\t last hellos:\t{rows}'

    return f'Hello, {name}!'


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
